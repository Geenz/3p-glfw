#!/bin/bash

cd "$(dirname "$0")"

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# load autobuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

stage="$(pwd)/stage"
    case "$AUTOBUILD_PLATFORM" in
        "windows")
			# Windows specific build steps.
            load_vsvars
            cmake .

			build_sln "GLFW.sln" "Debug|Win32" "glfw"
			build_sln "GLFW.sln" "Debug|Win32" "glfw"

			mkdir -p "$stage/lib/debug"
			mkdir -p "$stage/lib/release"

			mkdir -p "$stage/include"
        ;;
        "darwin")
			# Mac OS X specific build steps.
            		cmake -G "Xcode"

			xcodebuild -project GLFW.xcodeproj -target glfw -configuration Release
			xcodebuild -project GLFW.xcodeproj -target glfw -configuration Debug

			mkdir -p "$stage/lib/debug"
			mkdir -p "$stage/lib/release"

			cp "src/Release/libglfw3.a" \
				"$stage/lib/release/libglfw3.a"
			cp "src/Debug/libglfw3.a" \
				"$stage/lib/debug/libglfw3.a"

			mkdir -p "$stage/include"
			cp -R "include/" \
					"$stage/include/"
			
        ;;            
			
        "linux")
			# do release build
            CFLAGS="-m32 -O3" CXXFLAGS="-m32 -O3" ./configure --prefix="$stage" --includedir="$stage/include/glfw" --libdir="$stage/lib/release"
            make
            make install

			# clean the build artifacts
			make distclean

			# do debug build
            CFLAGS="-m32 -O0 -gstabs+" CXXFLAGS="-m32 -O0 -gstabs+" ./configure --prefix="$stage" --includedir="$stage/include/glfw" --libdir="$stage/lib/debug"
            make
            make install
        ;;
    esac
    mkdir -p "$stage/LICENSES"
    tail -n 31 COPYING.txt > "$stage/LICENSES/glfw.txt"

#pass

